# # import tkinter as tk
# # from tkVideoPlayer import TkinterVideo
 
# # root = tk.Tk()
 
# # videoplayer = TkinterVideo(master=root, scaled=True, pre_load=False)
# # videoplayer.load(r"samplevideo.mp4")
# # videoplayer.pack(expand=True, fill="both")
 
# # videoplayer.play() # play the video
 
# # root.mainloop()

# # Import required Libraries
# from tkinter import *
# from PIL import Image, ImageTk
# import cv2

# # Create an instance of TKinter Window or frame
# win= Tk()

# # Set the size of the window
# win.geometry("700x350")# Create a Label to capture the Video frames
# label =Label(win)
# label.grid(row=0, column=0)
# cap= cv2.VideoCapture('traffic.mp4')

# # Define function to show frame
# def show_frames():
#   # Get the latest frame and convert into Image
#   cv2image= cv2.cvtColor(cap.read()[1],cv2.COLOR_BGR2RGB)
#   img = Image.fromarray(cv2image)

#   # Convert image to PhotoImage
#   imgtk = ImageTk.PhotoImage(image = img)
#   label.imgtk = imgtk
#   label.configure(image=imgtk)

# # Repeat after an interval to capture continiously
# while True:
#   label.after(1, show_frames)

#   show_frames()
# win.mainloop()


import tkinter
import cv2
import PIL.Image, PIL.ImageTk
import time
# from cam_service_for_ws_PLC import stream_kafka

class App:
    def __init__(self, window, window_title, video_source = "con10.mp4"):
        self.window = window
        self.window.title(window_title)
        self.video_source = video_source

        # open video source (by default this will try to open the computer webcam)
        self.vid = MyVideoCapture(self.video_source)

        # Create a canvas that can fit the above video source size
        self.canvas = tkinter.Canvas(window, width = self.vid.width, height = self.vid.height)
        self.canvas.pack()
        self.canvas.place(relx = 0.45, rely=0.15)

        # Button that lets the user take a snapshot
        # self.btn_snapshot=tkinter.Button(window, text="Snapshot", width=50, command=self.snapshot)
        # self.btn_snapshot.pack(anchor=tkinter.CENTER, expand=True)
        # self.btn_snapshot.place(relx = 0.55, rely = 0.85)
        # After it is called once, the update method will be automatically called every delay milliseconds
        self.delay = 15
        self.update()

        self.window.mainloop()

    def snapshot(self):
        # Get a frame from the video source
        ret, frame = self.vid.get_frame()

        if ret:
            cv2.imwrite("frame-" + time.strftime("%d-%m-%Y-%H-%M-%S") + ".jpg", cv2.cvtColor(frame, cv2.COLOR_RGB2BGR))

    def update(self):
      # Get a frame from the video source
        ret, frame = self.vid.get_frame()


        if ret:
            self.photo = PIL.ImageTk.PhotoImage(image = PIL.Image.fromarray(frame))
            self.canvas.create_image(0, 0, image = self.photo, anchor = tkinter.NW)

        self.window.after(self.delay, self.update)


class MyVideoCapture:
    def __init__(self, video_source):
        # Open the video source
        self.video_source = video_source
        self.vid = cv2.VideoCapture(video_source)
        if not self.vid.isOpened():
            raise ValueError("Unable to open video source", video_source)

        # Get video source width and height
        self.width = self.vid.get(cv2.CAP_PROP_FRAME_WIDTH)
        self.height = self.vid.get(cv2.CAP_PROP_FRAME_HEIGHT)

    def get_frame(self):
        try:
            if self.vid.isOpened():
                ret, frame = self.vid.read()
                # time.sleep(2)
                frame_k = None
                if frame_k is None:
                    frame = cv2.resize(frame, (640,480))
                    return (True, cv2.cvtColor(frame, cv2.COLOR_BGR2RGB))
                    
                if True:
                    frame_k = cv2.resize(frame_k, (640,480))
                    # Return a boolean success flag and the current frame converted to BGR
                    return (True, cv2.cvtColor(frame_k, cv2.COLOR_BGR2RGB))
                else:
                    return (ret, None)
            else:
                return (ret, None)
        except:
            self.vid = cv2.VideoCapture(self.video_source)

    # Release the video source when the object is destroyed
    def __del__(self):
        if self.vid.isOpened():
            self.vid.release()

# Create a window and pass it to the Application object


