from tkinter import *
from turtle import width
from cam_service_for_ws_PLC import run
import pymongo
from kafka import KafkaConsumer
from pymongo import MongoClient
import ttk
import tkinter
from tkterminal import Terminal
from testapi import *
import threading
import os
import numpy as np
# import PIL.Image
# import PIL.ImageTk

# root = Toplevel()

# im = PIL.Image.open("bg.png")
# photo = PIL.ImageTk.PhotoImage(im)
KAFKA_BROKER_URL = "35.236.186.201:9092"
MONGO_SERVER_HOST = "35.236.186.201"
MONGO_SERVER_PORT = 26000
MONGO_DB = "LIVIS"
MONGO_COLLECTION_PARTS = "parts"
MONGO_COLLECTIONS = {MONGO_COLLECTION_PARTS: "parts"}

def singleton(cls):
    instances = {}

    def getinstance():
        if cls not in instances:
            instances[cls] = cls()
        return instances[cls]

    return getinstance


@singleton
class MongoHelper:
    client = None

    def __init__(self):
        if not self.client:
            self.client = MongoClient(
                host=MONGO_SERVER_HOST, port=MONGO_SERVER_PORT)
        self.db = self.client[MONGO_DB]

    def getDatabase(self):
        return self.db

    def getCollection(self, cname, create=False, codec_options=None, domain="lincode"):

        _DB = MONGO_DB
        DB = self.client[_DB]
        # make it as cname in [LIST OF COMMON COLLECTIONS]
        if cname == 'permissions':
            pass
        else:
            cname = domain + cname
        #print('collection_name for this request-------' ,cname )
        if cname in MONGO_COLLECTIONS:
            if codec_options:
                return DB.get_collection(MONGO_COLLECTIONS[cname], codec_options=codec_options)
            return DB[MONGO_COLLECTIONS[cname]]
        else:
            return DB[cname]



def get_parts():
    parts_col = MongoHelper().getCollection('parts',domain='livis')
    parts = [i for i in parts_col.find()]
    partlist = {}
    for part in parts:
        partlist[part["part_number"]] = str(part["_id"])
    return partlist



def func_print():
    print("something")

def switch(b1, b2):
    b1["state"] = "active"
    b2["state"] = "disable"

def value_check(ws, domain, part_dict, part_entered, mode): 
    try:
        part_id = part_dict[part_entered]
    except:
        error_text = Label(login_screen, text="Select valid part name", foreground='red')
        error_text.place(relx=0.175,rely=0.35)
        return False

    if ws == "" or domain == "" or part_id == "" or part_entered == "" or mode == "":
        error_text = Label(login_screen, text="Please enter all details", foreground='red')
        error_text.place(relx=0.175,rely=0.33)
        return False
    
    try:
        run(ws, domain, part_id, part_entered, mode)
    except Exception as e:
        print(e)


def LoginPage():

    # LOGIN SCREEN 
    global login_screen
    login_screen=Tk()
    login_screen.title("LIVIS Conveyor")
    login_screen.geometry("1280x720")
    login_screen.wm_attributes('-alpha', '0.5')
    # login_screen.configure(background='grey')
    # Add image file
    bg = PhotoImage( file = "bg3.png")
    # Show image using label
    #label1 = Label( login_screen, image=bg)
    label1 = Label( login_screen)
    label1.place(x = 0,y = 0)
    
    # Welcome test
    welcome_text = Label(login_screen, text="Please enter login details")
    welcome_text.place(relx=0.16,rely=0.1)
   
    # user id details 
    username_icon = Label(login_screen, text="User ID")
    username_icon.place(relx=0.1,rely=0.2)
    username_login_entry = Entry(login_screen, textvariable="username")
    username_login_entry.place(relx=0.2,rely=0.2)

    # workstation details   
    ws_icon = Label(login_screen, text="WS Name")
    ws_icon.place(relx=0.1,rely=0.25)
    ws__login_entry = Entry(login_screen, textvariable="workstation")
    ws__login_entry.place(relx= 0.2, rely=0.25)

    # part name details
    part_icon = Label(login_screen, text="Part name")
    part_icon.place(relx=0.1,rely=0.3)

    # part name list
    all_parts = get_parts()
    all_parts[""] = ""
    menu = StringVar()
    menu.set("")
    drop = ttk.Combobox(login_screen, width = 19, textvariable=menu, values=list(all_parts.keys()))
    drop.pack()
    drop.place(relx = 0.2 , rely = 0.3)

    # train mode or test mode
    type_icon = Label(login_screen, text="Mode")
    type_icon.place(relx=0.1,rely=0.35)
    # radio button 
    option = StringVar()
    R1 = Radiobutton(login_screen, text="Train", value="Train", var=option)
    R2 = Radiobutton(login_screen, text="Inference", value="Inference", var=option)
    R1.pack()
    R2.pack()
    R1.place(relx = 0.19, rely = 0.35)
    R2.place(relx=0.19, rely = 0.4)

    # License ddetails 
    license_text = open('license.txt','r').read().strip()
    display_license = "License Key     " + license_text 
    license_icon = Label(login_screen, text=display_license)
    license_icon.place(relx=0.12,rely=0.82)

    license_validity_icon = Label(login_screen, text="License Validity   01/01/2022 - 31/12/2022" )
    license_validity_icon.place(relx=0.12,rely=0.85)

    # terminal settings

    # global terminal
    # terminal = Terminal(height = 15, width = 55) # set state = DISABLED
    # terminal.shell = True
    # terminal.pack( fill='both')
    # terminal.place(relx=0.05, rely = 0.5)

    # Start button deatils
    submit_button = Button(login_screen, text="START", width=15, height=2, command=lambda : value_check(ws__login_entry.get(), username_login_entry.get(), all_parts, drop.get(), option.get()))
    submit_button.place(relx= 0.1, rely=0.47)

    # stop button details
    stop_button = Button(login_screen, state="disabled", text="Stop",  width=15, height=2, command=lambda : exit(1))
    stop_button.place(relx= 0.25, rely=0.47)
    # exit button details
    exit_button = Button(login_screen, text="Exit", command=login_screen.destroy, width = 5)
    # exit_button.pack(pady=20)
    exit_button.place(relx= 0.2, rely=0.57)
    
    # playing video
    t1 = threading.Thread(target=App(login_screen, "test"))
    t1.start()
    login_screen.wm_attributes("-alpha", 1)
    login_screen.mainloop()
LoginPage()
