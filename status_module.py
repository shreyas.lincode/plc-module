class Status():
    def __init__(self,plc,address,accept = 1,reject = 2):
        self.plc = plc.plc
        self.address = address
        self.state_vals={}
        self.state_vals['accept'] = accept
        self.state_vals['reject'] = reject

    def write_accept(self):
        self.plc.write_holding_register(self.address,self.state_vals['accept'])

    def write_reject(self):
        self.plc.write_holding_register(self.address,self.state_vals['reject'])