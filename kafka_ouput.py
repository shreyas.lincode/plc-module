import cv2
from kafka import KafkaProducer, KafkaConsumer
import numpy as np

KAFKA_BROKER_URL = "35.236.186.201:9092"

# topic = workstation_id + "_" + camera_id+ "_o"
topic = "62171fd45d3e95fd7dff29e1_0_o"
print(topic)
consumer1 = KafkaConsumer(topic, bootstrap_servers=KAFKA_BROKER_URL, auto_offset_reset='latest', session_timeout_ms=3000)
for message in consumer1:
    img_inference = cv2.imdecode(np.frombuffer(message.value, np.uint8), 1)

    # cv2.imshow("frame", img_inference)
    print(img_inference.shape)

    key = cv2.waitKey(1)
    
    if key == ord('q'):
        break
        # a = message.value.decode('utf-8')
        # b = json.loads(a)

        # for item in b['predictions']:
        #    detections.append(item['label'])
        
        # label_list = ast.literal_eval(message.key.decode('UTF-8')) 
        # for item in label_list:
        #    detections.append(item['label'])

        # im_b64_str = b["frame"]
        # im_b64 = bytes(im_b64_str[2:], 'utf-8')
        # im_binary = base64.b64decode(im_b64)
        # im_arr = np.frombuffer(im_binary, dtype=np.uint8)
        # img_inference = cv2.imdecode(im_arr, flags=cv2.IMREAD_COLOR)
        #write the inference image on disk and get http path
        
        # saved_images_inference.append(save_inference_image(img_inference, part_id, True, part_id))