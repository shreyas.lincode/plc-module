from arena_api.system import system
from arena_api.buffer import *
from arena_api import enums
import ctypes
import numpy as np
import cv2
import time
import threading
import sys
import redis
import pickle
# from inference_module import *
# from config_module import *



def singleton(cls):
    """
    This is a decorator which helps to create only 
    one instance of an object in the particular process.
    This helps the preserve the memory and prevent unwanted 
    creation of objects.
    """
    instances = {}
    def getinstance():
        if cls not in instances:
            instances[cls] = cls()
        return instances[cls]
    return getinstance


@singleton    
class RedisKeyBuilderWorkstation():
    def __init__(self):
        # self.wid = get_workstation_id('livis//workstation_settings//settings_workstation.json')
        self.workstation_name = "WS_01" #get_workstation_by_id(self.wid)
    def get_key(self, camera_id, identifier):
        return "{}_{}_{}".format(self.workstation_name, str(camera_id), identifier)


import pickle

@singleton
class CacheHelper():
    def __init__(self):
        # self.redis_cache = redis.StrictRedis(host="164.52.194.78", port="8080", db=0, socket_timeout=1)
        self.redis_cache = redis.StrictRedis(host="localhost", port="6379", db=0, socket_timeout=1)
        # settings.REDIS_CLIENT_HOST
        print("REDIS CACHE UP!")

    def get_redis_pipeline(self):
        return self.redis_cache.pipeline()
    
    #should be {'key'  : 'value'} always
    def set_json(self, dict_obj):
        try:
            k, v = list(dict_obj.items())[0]
            v = pickle.dumps(v)
            return self.redis_cache.set(k, v)
        except redis.ConnectionError:
            return None

    def get_json(self, key):
        try:
            temp = self.redis_cache.get(key)
            #print(temp)\
            if temp:
                temp= pickle.loads(temp)
            return temp
        except redis.ConnectionError:
            return None
        return None

    def execute_pipe_commands(self, commands):
        #TBD to increase efficiency can chain commands for getting cache in one go
        return None


def get_ip(device1):
    raw_ip = device1.nodemap['GevCurrentIPAddress'].value
    ip_list = []
    for i in range(4):
        temp = str(int(raw_ip/(256**(3-i))%256))
        ip_list.append(temp)
    # print(ip_list)
    final_ip = ip_list[0]+'.'+ip_list[1]+'.'+ip_list[2]+'.'+ip_list[3]
    # return {final_ip:raw_ip}
    return final_ip

def get_ser(device1):
    ser = device1.nodemap['DeviceSerialNumber'].value
    print(device1)
    print(ser)
    return ser

tries = 0
tries_max = 6
sleep_time_secs = 10
while tries < tries_max:  # Wait for device for 60 seconds
    devices = system.create_device()
    if not devices:
        print(
            f'Try {tries+1} of {tries_max}: waiting for {sleep_time_secs} '
            f'secs for a device to be connected!')
        for sec_count in range(sleep_time_secs):
            time.sleep(1)
            print(f'{sec_count + 1 } seconds passed ',
                  '.' * sec_count, end='\r')
        tries += 1
    else:
        print(f'Created {len(devices)} device(s)\n')
        device = devices[0]
        break
else:
    raise Exception(f'No device found! Please connect a device and run '
                    f'the example again.')

camera_ips = {'169.254.134.12':'bottom'}#,'192.168.1.41':'top','192.168.1.35':'side'}
# camera_ips = {'192.168.1.41':'top','192.168.1.35':'side'}
cameras_dict = {}
for d in devices:
    # temp = get_ip(d)
    temp = get_ser(d)
    cameras_dict[camera_ips[temp]] = d

print(cameras_dict)


initial_acquisition_mode_list = []

for device in devices:
    nodemap = device.nodemap
    initial_acquisition_mode_list.append(nodemap.get_node("AcquisitionMode").value)
    nodemap.get_node("AcquisitionMode").value = "Continuous"
    tl_stream_nodemap = device.tl_stream_nodemap
    tl_stream_nodemap["StreamBufferHandlingMode"].value = "NewestOnly"
    tl_stream_nodemap['StreamAutoNegotiatePacketSize'].value = True
    tl_stream_nodemap['StreamPacketResendEnable'].value = True
    nodes = nodemap.get_node(['PixelFormat'])
    nodes['PixelFormat'].value = 'RGB8'


CacheHelper().set_json({"input_frame":[]})
while 1:
    for cam_pos,device in cameras_dict.items():
        with device.start_stream(1):
        
            buffer = device.get_buffer()
            item = BufferFactory.copy(buffer)
            device.requeue_buffer(buffer)
            npndarray = np.ctypeslib.as_array(item.pdata,shape=(item.height, item.width, 3)).reshape(item.height, item.width, 3)
            npndarray = cv2.cvtColor(npndarray, cv2.COLOR_BGR2RGB)
            npndarray = cv2.resize(npndarray,(1280,720))
            # print(device.nodemap["GevCurrentIPAddress"].value)
            # print((device.nodemap["GevCurrentIPAddress"].value).type)
            # ip_dict = get_ip(device) 
            # cv2.imshow(device.nodemap["DeviceSerialNumber"].value, npndarray)
            frame = npndarray

            
            value = CacheHelper().get_json("trigg")
            try:
                if value:
                    # cv2.imwrite('image/pred.jpg',frame)
                    print("triggered!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!")
                    CacheHelper().set_json({"input_frame":frame})
                    CacheHelper().set_json({"trigg":None})
                    CacheHelper().set_json({'part_status':'accepted'})
                    print("wrote frame ",frame.shape)
                    cv2.imshow('cam_pos', frame)
                    
                    # break
                else:
                    pass
            except Exception as e:
                print(e)

            
            # CacheHelper().set_json({"frame":frame})
            # if cam_pos == 'side':
            #     # pass
            #     frame = cv2.flip(frame, 0)
            # else:
            
            BufferFactory.destroy(item)  
            if cv2.waitKey(1) == ord('q'):
                break
        # # device.stop_stream()
    
        