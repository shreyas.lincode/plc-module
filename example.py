import plc_module
import Button
import status_module
import sys

ip = '192.168.1.50'

plc = plc_module.PLC(ip)
status = plc.check_connection()
if not status:
    sys.exit(0)

process_running = Button.Button(plc,24,0,1)
trig = Button.Button(plc,2,0,1)
emer = Button.Button(plc,8,1,0)
status = status_module.Status(plc,23,1,2)

while 1:
    if process_running.get_status()=='active':
        if trig.get_stataus()=='active':
            print('Capture Image')
            s = input('Enter part status\n1:Accept\n2:Reject\nSelect one of the above options:')
            if s=='1':
                status.write_accept()
            elif s=='2':
                status.write_reject()